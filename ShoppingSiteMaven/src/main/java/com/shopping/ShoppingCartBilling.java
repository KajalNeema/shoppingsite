package com.shopping;
import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class ShoppingCartBilling {

	public static void main(String[] args) {  

		try {  
			//Get Shopping Cart Data from XML and convert it into the respective Java Objects

			File file = new File("src\\main\\resources\\shoppingcart.xml");  
			JAXBContext jaxbContext = JAXBContext.newInstance(ShoppingCart.class);  

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
			ShoppingCart cart= (ShoppingCart) jaxbUnmarshaller.unmarshal(file);

			System.out.println(cart);

			List<Item> list=cart.getItems();

			jaxbContext = JAXBContext.newInstance(Categories.class);  
			file = new File("src\\main\\resources\\categories.xml");  
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
			Categories categories= (Categories) jaxbUnmarshaller.unmarshal(file);
			List<Category> listOfCategories=categories.getCategory();  

			jaxbContext = JAXBContext.newInstance(FlatDiscountSlabs.class);  
			file = new File("src\\main\\resources\\slabs.xml");  
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
			FlatDiscountSlabs discountSlabs= (FlatDiscountSlabs) jaxbUnmarshaller.unmarshal(file);
			List<Slab> slabs  =discountSlabs.getSlab(); 


			//Conversion Ends Here



			double grandTotal=printItemizedBill(list,listOfCategories);
			printGrandTotalBill(grandTotal, slabs);

		}catch (JAXBException e) {  
			e.printStackTrace();  
		}  
	}



	public static double printItemizedBill(List<Item> itemList,List<Category> listOfCategories)
	{

		HashMap<String,Category> categoriesMap=new HashMap<String,Category>();
		for(Category category:listOfCategories)
		{
			categoriesMap.put(category.getId(), category);
		}

		System.out.println("Item Id "+" "+"Item Name"+"  "+"Unit Price"+" "+"quantity"+" "+"total price"+"Discount"+ "Net Price");
		float discountAmount=0.0f;
		Category category=null;

		double grandTotal=0.0d;
		for(Item item:itemList)  
		{
			category= categoriesMap.get(item.getItemCategoryID());
			discountAmount=(item.getQuantity()*item.getUnitPrice()*category.getDiscPerc())/100;
			item.setDiscountAmount(discountAmount); 
			item.setPriceAfterDiscount((item.getQuantity()*item.getUnitPrice())-item.getDiscountAmount());;
			System.out.println(item.getItemID()+" "+item.getItemName()+"  "+item.getUnitPrice()+" "+item.getQuantity()+" "+item.getQuantity()*item.getUnitPrice()+" "+item.getDiscountAmount()+ " "+ item.getPriceAfterDiscount());
			grandTotal=grandTotal+item.getPriceAfterDiscount();
		}

		System.out.println("Total Price"+grandTotal);
		return grandTotal;
	}




	public static double printGrandTotalBill(double grandTotal,List<Slab> slabs)
	{
		int totalDisountOnGrandTotal=0;
		for(Slab slab:slabs)
		{
			if(slab.getRangeMin()!=null && slab.getRangeMax()!=null && slab.getRangeMax().trim().length()!=0 && slab.getRangeMin().trim().length()!=0 )
			{
				int rangeMin=Integer.parseInt(slab.getRangeMin());
				int rangeMax=Integer.parseInt(slab.getRangeMax());
				if(grandTotal>=rangeMin && grandTotal<rangeMax)
				{
					totalDisountOnGrandTotal=slab.getDiscPerc();
				}


			}
			if(slab.getRangeMin()==null && slab.getRangeMax()!=null && slab.getRangeMax().trim().length()!=0)
			{

				int rangeMax=Integer.parseInt(slab.getRangeMax());
				if( grandTotal<rangeMax)
				{
					totalDisountOnGrandTotal=slab.getDiscPerc();
				}


			}
			if(slab.getRangeMin()!=null && slab.getRangeMax()==null && slab.getRangeMin().trim().length()!=0)
			{

				int rangeMin=Integer.parseInt(slab.getRangeMin());
				if(grandTotal>=rangeMin)
				{
					totalDisountOnGrandTotal=slab.getDiscPerc();
				}


			}
		}
		double applicableDiscount=((grandTotal*totalDisountOnGrandTotal)/100);
		double netBill=grandTotal-applicableDiscount;
		System.out.println("Applicable discount"+applicableDiscount);
		System.out.println("Net bill"+netBill);
		return netBill;





	}
}

