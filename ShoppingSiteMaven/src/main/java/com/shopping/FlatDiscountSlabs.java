package com.shopping;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class FlatDiscountSlabs {

	List<Slab> slab;

	@XmlElement
	public List<Slab> getSlab() {
		return slab;
	}

	public void setSlab(List<Slab> slab) {
		this.slab = slab;
	}



}
