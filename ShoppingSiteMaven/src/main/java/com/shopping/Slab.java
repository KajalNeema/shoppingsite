package com.shopping;

public class Slab {

	private  String RangeMin;
	private String RangeMax;
	private int discPerc;
	public String getRangeMin() {
		return RangeMin;
	}
	public void setRangeMin(String rangeMin) {
		RangeMin = rangeMin;
	}
	public String getRangeMax() {
		return RangeMax;
	}
	public void setRangeMax(String rangeMax) {
		RangeMax = rangeMax;
	}
	public int getDiscPerc() {
		return discPerc;
	}
	public void setDiscPerc(int discPerc) {
		this.discPerc = discPerc;
	}



}
