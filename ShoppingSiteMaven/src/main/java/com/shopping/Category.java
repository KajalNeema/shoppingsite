package com.shopping;

public class Category {
	private String id;
	private String name;
	private int discPerc;

	public	Category()
	{

	}
	public Category(String id, String name, int discPerc) {
		super();
		this.id = id;
		this.name = name;
		this.discPerc = discPerc;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDiscPerc() {
		return discPerc;
	}
	public void setDiscPerc(int discPerc) {
		this.discPerc = discPerc;
	}




}
