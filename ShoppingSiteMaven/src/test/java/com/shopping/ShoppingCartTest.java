package com.shopping;
import static org.junit.Assert.*;
import java.io.File;
import java.util.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

public class ShoppingCartTest {

	@Test
	public void testprintItemizedBill() {

		List<Item> cartList=getShoppingCartList();
		List<Category> listOfCategories=getCategories();

		double result=1141.9;
		assertEquals(result, ShoppingCartBilling.printItemizedBill(cartList, listOfCategories),0.1);


	}
	@Test
	public void testGrandTotal() {

		List<Item> cartList=getShoppingCartList();
		List<Slab> slabs=getSlabs();
		List<Category> listOfCategories=getCategories();
		double expected=1119.0;
		double grandTotal=(ShoppingCartBilling.printItemizedBill(cartList, listOfCategories));
		double netBill=ShoppingCartBilling.printGrandTotalBill(grandTotal, slabs);
		assertEquals(expected,netBill,0.1);
	}

	public List<Item>  getShoppingCartList()
	{
		try
		{
			File file = new File("src\\test\\resources\\shoppingcart.xml");  
			JAXBContext jaxbContext = JAXBContext.newInstance(ShoppingCart.class);  
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
			ShoppingCart cart= (ShoppingCart) jaxbUnmarshaller.unmarshal(file);
			System.out.println(cart);
			return cart.getItems();

		}catch(JAXBException j)
		{
			j.printStackTrace();
		}
		return null;
	}
	public List<Category> getCategories()
	{
		try
		{
			JAXBContext  jaxbContext = JAXBContext.newInstance(Categories.class);  
			File file = new File("src\\test\\resources\\categories.xml");  
			Unmarshaller  jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
			Categories categories= (Categories) jaxbUnmarshaller.unmarshal(file);
			return categories.getCategory();  
		}catch(JAXBException j)
		{
			j.printStackTrace();
		}
		return null;

	}

	public List<Slab> getSlabs()
	{
		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(FlatDiscountSlabs.class);  
			File  file = new File("src\\test\\resources\\slabs.xml");  
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
			FlatDiscountSlabs discountSlabs= (FlatDiscountSlabs) jaxbUnmarshaller.unmarshal(file);
			return discountSlabs.getSlab();

		}catch(JAXBException j)
		{
			j.printStackTrace();
		}
		return null;

	}

}
